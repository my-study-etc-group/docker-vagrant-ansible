# docker-vagrant-ansible

provision and up vagrant with docker

## up and provision

```
vagrant up
```

## ssh

```
vagrant ssh
```

## stop

```
vagrant halt
```

## destroy

```
vagrant destroy
```
